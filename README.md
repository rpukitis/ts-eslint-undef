# npx eslint index.js

will throw errors

```
  5:18   error  'IValue' is not defined  no-undef
  6:3    error  'a' is not defined       no-undef
  7:3    error  'b' is not defined       no-undef
  10:13  error  'Value' is not defined   no-undef
  10:21  error  'IValue' is not defined  no-undef
```

# npx babel index.ts --out-file index.js

will strip away typescript interfaces/types

```javascript
  export function add(a, b) {
    return a + b;
  }
  export function split(value) {
    return [value.a, value.b];
  }
  export function join(value) {
    return `${value.a} ${value.b}`;
  }
```
