export function add(a: number, b: number) {
  return a + b
}

export interface IValue {
  a: string;
  b: string;
}

export type Value = IValue

export function split(value: IValue) {
  return [value.a, value.b]
}

export function join(value: Value) {
  return `${value.a} ${value.b}`
}
